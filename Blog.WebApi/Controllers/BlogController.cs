﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blog.BLL.Interfejsi;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Blog.Model.Modeli;

namespace Blog.WebApi.Controllers
{
    [Route("api/[action]")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private IBlog _blogServis;
        public BlogController(IBlog blogServis)
        {
            _blogServis = blogServis;
        }


        [HttpGet]
        [ActionName("tags")]
        public ActionResult tags()
        {
            var lista = _blogServis.VratiTagove();
            return Ok(lista);
        }

        [HttpGet]
        [ActionName("posts")]
        public ActionResult posts(string tag = "")
        {
            var lista = _blogServis.VratiPostove(tag);
            return Ok(lista);
        }

        [HttpGet("{slug}")]
        [ActionName("posts")]
        public ActionResult Get(string slug)
        {
            var lista = _blogServis.VratiPost(slug);
            return Ok(lista);
        }

        [HttpPost]
        [ActionName("posts")]
        public string Post([FromBody]BlogPostSingle blogPost)
        {
            var post = _blogServis.KreirajPost(blogPost);
            return post;
        }

        [HttpPut("{slug}")]
        [ActionName("posts")]
        public string Put(string slug, [FromBody]BlogPostSingle blogPost)
        {
            var post = _blogServis.EditujPost(slug, blogPost);
            return post;
        }

        [HttpDelete("{slug}")]
        [ActionName("posts")]
        public ActionResult Delete(string slug)
        {
            var post = _blogServis.ObrisiPost(slug);
            return Ok(post);
        }

    }
}