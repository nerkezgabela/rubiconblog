USE [master]
GO
/****** Object:  Database [BlogDB]    Script Date: 3/5/2020 10:09:10 PM ******/
CREATE DATABASE [BlogDB]
 CONTAINMENT = NONE
 GO
ALTER DATABASE [BlogDB] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BlogDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BlogDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BlogDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BlogDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BlogDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BlogDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [BlogDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BlogDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BlogDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BlogDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BlogDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BlogDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BlogDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BlogDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BlogDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BlogDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [BlogDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BlogDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BlogDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BlogDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BlogDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BlogDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BlogDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BlogDB] SET RECOVERY FULL 
GO
ALTER DATABASE [BlogDB] SET  MULTI_USER 
GO
ALTER DATABASE [BlogDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BlogDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BlogDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BlogDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BlogDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BlogDB', N'ON'
GO
ALTER DATABASE [BlogDB] SET QUERY_STORE = OFF
GO
USE [BlogDB]
GO
/****** Object:  Table [dbo].[Post]    Script Date: 3/5/2020 10:09:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Post](
	[PostId] [int] IDENTITY(1,1) NOT NULL,
	[Slug] [nvarchar](200) NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](1500) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[UpdatedAt] [datetime] NULL,
 CONSTRAINT [PK_Post] PRIMARY KEY CLUSTERED 
(
	[PostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PostTags]    Script Date: 3/5/2020 10:09:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostTags](
	[PostId] [int] NOT NULL,
	[TagId] [int] NOT NULL,
 CONSTRAINT [PK_PostTags] PRIMARY KEY CLUSTERED 
(
	[PostId] ASC,
	[TagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tag]    Script Date: 3/5/2020 10:09:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tag](
	[TagId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED 
(
	[TagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Post] ON 

INSERT [dbo].[Post] ([PostId], [Slug], [Title], [Description], [Body], [CreatedAt], [UpdatedAt]) VALUES (1, N'internet-trends-2018', N'Internet Trends 2018', N'Ever wonder how?', N'An opinionated commentary, of the most important presentation of the year', CAST(N'2020-03-05T21:53:19.347' AS DateTime), NULL)
INSERT [dbo].[Post] ([PostId], [Slug], [Title], [Description], [Body], [CreatedAt], [UpdatedAt]) VALUES (2, N'augmented-reality-ios-application', N'Augmented Reality iOS Application', N'Rubicon Software Development and Gazzda furniture are proud to launch an augmented reality app.', N'The app is simple to use, and will help you decide on your best furniture fit.', CAST(N'2020-03-05T21:55:14.957' AS DateTime), NULL)
INSERT [dbo].[Post] ([PostId], [Slug], [Title], [Description], [Body], [CreatedAt], [UpdatedAt]) VALUES (3, N'augmented-reality-testing-data', N'Augmented Reality Testing Data', N'Rubicon Software Development and Puma', N'The app is simple to use.', CAST(N'2020-03-05T21:56:23.477' AS DateTime), NULL)
INSERT [dbo].[Post] ([PostId], [Slug], [Title], [Description], [Body], [CreatedAt], [UpdatedAt]) VALUES (4, N'prepared-do-an-dissuade-be-so-whatever-steepest', N'Prepared do an dissuade be so whatever steepest.', N'Yet her beyond looked either day wished nay. By doubtful disposed do juvenile an.', N'Parish so enable innate in formed missed.', CAST(N'2020-03-05T21:59:58.727' AS DateTime), NULL)
INSERT [dbo].[Post] ([PostId], [Slug], [Title], [Description], [Body], [CreatedAt], [UpdatedAt]) VALUES (5, N'promotion-an-ourselves-up-otherwise-my', N'Promotion an ourselves up otherwise my.', N'High what each snug rich far yet easy.', N'Terminated as middletons or by instrument.', CAST(N'2020-03-05T22:01:05.183' AS DateTime), NULL)
INSERT [dbo].[Post] ([PostId], [Slug], [Title], [Description], [Body], [CreatedAt], [UpdatedAt]) VALUES (6, N'become-latter-but-nor-abroad-wisdom-waited', N'Become latter but nor abroad wisdom waited.', N'Dashwoods see frankness objection abilities.', N'Gentleman he september in oh excellent.', CAST(N'2020-03-05T22:02:41.987' AS DateTime), NULL)
INSERT [dbo].[Post] ([PostId], [Slug], [Title], [Description], [Body], [CreatedAt], [UpdatedAt]) VALUES (7, N'battle-angel', N'Battle Angel.', N'Dashwoods see frankness objection abilities.', N'Based on a manga of the same name.', CAST(N'2020-03-05T22:07:06.360' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Post] OFF
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (1, 1)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (1, 2)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (1, 3)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (1, 4)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (1, 5)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (1, 6)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (1, 7)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (2, 8)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (2, 9)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (3, 8)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (3, 10)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (4, 11)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (4, 12)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (5, 13)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (5, 14)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (6, 15)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (6, 16)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (6, 17)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (7, 18)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (7, 19)
INSERT [dbo].[PostTags] ([PostId], [TagId]) VALUES (7, 20)
SET IDENTITY_INSERT [dbo].[Tag] ON 

INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (3, N'2018')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (11, N'Addidas')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (4, N'angular')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (16, N'Apple')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (9, N'AR')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (20, N'China')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (18, N'CL')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (6, N'CSS')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (19, N'Ford')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (5, N'HTTP')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (2, N'innovation')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (8, N'iOS')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (7, N'JavaScript')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (10, N'MU')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (12, N'Nike')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (15, N'NY')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (14, N'Olovo')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (13, N'Paris')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (17, N'Rain')
INSERT [dbo].[Tag] ([TagId], [Name]) VALUES (1, N'trends')
SET IDENTITY_INSERT [dbo].[Tag] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [Unique_Slug]    Script Date: 3/5/2020 10:09:10 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Unique_Slug] ON [dbo].[Post]
(
	[Slug] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [Unique_Name]    Script Date: 3/5/2020 10:09:10 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Unique_Name] ON [dbo].[Tag]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PostTags]  WITH CHECK ADD  CONSTRAINT [FK_PostTags_Post] FOREIGN KEY([PostId])
REFERENCES [dbo].[Post] ([PostId])
GO
ALTER TABLE [dbo].[PostTags] CHECK CONSTRAINT [FK_PostTags_Post]
GO
ALTER TABLE [dbo].[PostTags]  WITH CHECK ADD  CONSTRAINT [FK_PostTags_Tag] FOREIGN KEY([TagId])
REFERENCES [dbo].[Tag] ([TagId])
GO
ALTER TABLE [dbo].[PostTags] CHECK CONSTRAINT [FK_PostTags_Tag]
GO
USE [master]
GO
ALTER DATABASE [BlogDB] SET  READ_WRITE 
GO
