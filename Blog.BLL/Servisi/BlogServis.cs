﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blog.BLL.Interfejsi;
using Blog.Model.DBModels;
using Blog.Model.Modeli;
using Blog.BLL.Pomoc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Blog.BLL.Servisi
{
    public class BlogServis : IBlog
    {
        private AppDBContext _context;
        public BlogServis(AppDBContext context)
        {
            _context = context;
        }


        public Tags VratiTagove()
        {
            var tagovi = _context.Tag.Select(x => x.Name).ToList();
            var tags = new Tags();
            tags.tags = tagovi;
            return tags;
        }
        public BlogPost VratiPostove(string tag)
        {
            var postovi = new List<PostModel1>();
            var bp = new BlogPost();
            if (tag != "")
            {
                var listaDB = (from p in _context.Post.ToList()
                               join pt in _context.PostTags.ToList() on
                               p.PostId equals pt.PostId
                               join t in _context.Tag.ToList() on
                               pt.TagId equals t.TagId
                               where t.Name == tag
                               select new PostModel
                               {
                                   PostId = p.PostId,
                                   Slug = p.Slug,
                                   Title = p.Title,
                                   Description = p.Description,
                                   Body = p.Body,
                                   CreatedAt = p.CreatedAt,
                                   UpdatedAt = p.UpdatedAt
                               }).ToList().OrderByDescending(x => x.CreatedAt); 
                if (listaDB != null)
                {
                    foreach(var p in listaDB)
                    {
                        var pm = new PostModel1();
                        pm.Slug = p.Slug;
                        pm.Title = p.Title;
                        pm.Description = p.Description;
                        pm.Body = p.Body;
                        pm.CreatedAt = p.CreatedAt;
                        pm.UpdatedAt = p.UpdatedAt;
                        pm.TagList = (from pt in _context.PostTags.ToList()
                                      join t in _context.Tag.ToList() on
                                      pt.TagId equals t.TagId
                                      where pt.PostId == p.PostId
                                      select  t.Name).ToList();
                        postovi.Add(pm);
                    }
                    bp.blogPosts = postovi;
                    bp.postsCount = postovi.Count;
                }
                
            }
            else
            {
                var postoviDB = _context.Post.ToList();
                if (postoviDB != null)
                {
                    foreach (var p in postoviDB)
                    {
                        var pm = new PostModel1();
                        pm.Slug = p.Slug;
                        pm.Title = p.Title;
                        pm.Description = p.Description;
                        pm.Body = p.Body;
                        pm.CreatedAt = p.CreatedAt;
                        pm.UpdatedAt = p.UpdatedAt;
                        pm.TagList = (from pt in _context.PostTags.ToList()
                                      join t in _context.Tag.ToList() on
                                      pt.TagId equals t.TagId
                                      where pt.PostId == p.PostId
                                      select t.Name).ToList();
                        postovi.Add(pm);
                    }
                  
                    bp.blogPosts = postovi;
                    bp.postsCount = postovi.Count;
                }
            }
            return bp;
        }
        public string VratiPost(string slug)
        {
            var p = _context.Post.Where(x => x.Slug == slug).FirstOrDefault();
            if (p == null)
                return "Post sa ovim slugom ne postoji!";
            var pms = new BlogPostSingle1();
            var pm = new PostModel1();
            pm.Slug = p.Slug;
            pm.Title = p.Title;
            pm.Description = p.Description;
            pm.Body = p.Body;
            pm.CreatedAt = p.CreatedAt;
            pm.UpdatedAt = p.UpdatedAt;
            pm.TagList = (from pt in _context.PostTags.ToList()
                          join t in _context.Tag.ToList() on
                          pt.TagId equals t.TagId
                          where pt.PostId == p.PostId
                          select t.Name).ToList();
            pms.blogPost = pm;
            var post = JsonConvert.SerializeObject(pms, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            return post;
        }
        public string KreirajPost(BlogPostSingle blogPost)
        {
            var post = new Post();
            var listaTagova = blogPost.blogPost.TagList;           
            post.Slug = NapraviSlug.USlug(blogPost.blogPost.Title);
            var testSlug = _context.Post.Where(x => x.Slug == post.Slug).FirstOrDefault();
            if (testSlug != null)
                return "Post sa ovim naslovom vec postoji u bazi!";
            post.Title = blogPost.blogPost.Title;
            post.Description = blogPost.blogPost.Description;
            post.Body = blogPost.blogPost.Body;
            post.CreatedAt = DateTime.Now;
            _context.Post.Add(post); 
            _context.SaveChanges();
            var postId = post.PostId;
            if (listaTagova != null)
            {
                var listaTagovaDB = _context.Tag.Select(x => x.Name).ToList();
                    foreach (var t in listaTagova)
                    {
                        if (!listaTagovaDB.Contains(t))
                        {
                            var tag = new Tag();
                            var postTag = new PostTags();
                            tag.Name = t;
                            _context.Tag.Add(tag);
                            _context.SaveChanges();
                            postTag.PostId = postId;
                            postTag.TagId = tag.TagId;
                            _context.PostTags.Add(postTag);
                            _context.SaveChanges();
                        }
                        else
                        {
                            var postTag = new PostTags();
                            var tagId = (_context.Tag.Where(x => x.Name == t).FirstOrDefault()).TagId;
                            postTag.PostId = postId;
                            postTag.TagId = tagId;
                            _context.PostTags.Add(postTag);
                            _context.SaveChanges();
                        }
                    }              
            }
            return VratiPost(post.Slug);
        }
        public string EditujPost(string slug, BlogPostSingle blogPost)
        {
            var noviSlug = NapraviSlug.USlug(blogPost.blogPost.Title);
            if (slug != noviSlug)
            {
                var testNoviPost = _context.Post.Where(x => x.Slug == noviSlug).FirstOrDefault();
                if (testNoviPost != null)
                    return "Post sa ovim novim naslovom vec postoji u bazi!";
            }
           
            var testPost = _context.Post.Where(x => x.Slug == slug).FirstOrDefault();
            if (testPost == null)
                return "Post sa ovim slugom ne postoji!";

            if (testPost.Title != blogPost.blogPost.Title)
            {
                testPost.Slug = noviSlug;
                testPost.Title = blogPost.blogPost.Title;
            }
            if (blogPost.blogPost.Description != null)
                testPost.Description = blogPost.blogPost.Description;

            _context.SaveChanges();
            return VratiPost(testPost.Slug);
        }
        public string ObrisiPost(string slug)
        {
            var post = _context.Post.Where(x => x.Slug == slug).FirstOrDefault();
            if (post == null)
                return "Post sa ovim slugom ne postoji u bazi!";
            var listaTagova = _context.PostTags.Where(x => x.PostId == post.PostId).ToList();
            if (listaTagova != null)
            {
                foreach(var t in listaTagova)
                {
                    _context.PostTags.Remove(t);
                }
                _context.SaveChanges();
            }
            _context.Post.Remove(post);
            _context.SaveChanges();
            return "Post sa slugom:" + slug + " sa pripadajućim tagovima je obrisan!";
        }

    }
}
