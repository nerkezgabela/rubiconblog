﻿using Blog.Model.Modeli;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.BLL.Interfejsi
{
    public interface IBlog
    {
        Tags VratiTagove();
        string VratiPost(string slug);
        BlogPost VratiPostove(string tag);
        string KreirajPost(BlogPostSingle blogPost);
        string EditujPost(string slug, BlogPostSingle blogPost);
        string ObrisiPost(string slug);
    }
}
