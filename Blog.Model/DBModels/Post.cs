﻿using System;
using System.Collections.Generic;

namespace Blog.Model.DBModels
{
    public partial class Post
    {
        public Post()
        {
            PostTags = new HashSet<PostTags>();
        }

        public int PostId { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public virtual ICollection<PostTags> PostTags { get; set; }
    }
}
