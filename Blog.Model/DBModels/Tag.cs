﻿using System;
using System.Collections.Generic;

namespace Blog.Model.DBModels
{
    public partial class Tag
    {
        public Tag()
        {
            PostTags = new HashSet<PostTags>();
        }

        public int TagId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PostTags> PostTags { get; set; }
    }
}
