﻿using System;
using System.Collections.Generic;

namespace Blog.Model.DBModels
{
    public partial class PostTags
    {
        public int PostId { get; set; }
        public int TagId { get; set; }

        public virtual Post Post { get; set; }
        public virtual Tag Tag { get; set; }
    }
}
