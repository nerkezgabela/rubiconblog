﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Model.Modeli
{
    public class BlogPostSingle1
    {
        public PostModel1 blogPost { get; set; }
    }

    public class BlogPostSingle
    {
        public PostModel blogPost { get; set; }
    }
    public class BlogPost
    {
        public List<PostModel1> blogPosts { get; set; }
        public int postsCount { get; set; }
    }
    public class PostModel
    {
        public int PostId { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public List<string> TagList { get; set; }
    }

    public class PostModel1
    {
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public List<string> TagList { get; set; }
    }
}
